How to build the web site
=========================

You need hugo to build the website.  On Debian, install it from the
'hugo' package:

    # apt install hugo

Furthermore, you need to checkout the submodules:

    sequoia-web $ git submodule init
    sequoia-web $ git submodule update

Now you can build the site using 'make', it will be put into 'public'.
To deploy the web site, use 'make deploy
TARGET=some.host.tld:target/path'.

Hacking
-------

We modified some parts of the theme.  To see the modifications, run:

    % find layouts -type f | while read F ; do if [ -e themes/*/$F ] ; then diff -u themes/*/$F $F ; fi ; done
