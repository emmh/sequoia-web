+++
title = "Holistic Approach to OpenPGP Security"
keywords = ["holistic", "community", "ecosystem", "integration", "standardization"]
+++

Improving the security of OpenPGP users requires more than a new
implementation.  Therefore, we are taking a holistic approach and are
improving the ecosystem.

Our efforts in this regards always start with users.  We have talked
extensively with users to find out what their needs are, what their
workflows are, what tools they currently use, and what problems they
have.  We spoke to application developers, digital security trainers,
administrators, people in charge of operational security, and end
users.  We build tools based on what we learn from our discussions
with them.  In fact, we involve our users closely in the development
process.

For example, we are developing [OpenPGP CA](/projects/#openpgp-ca)
with [OCCRP](https://www.occrp.org/).  OpenPGP CA is a tool for
managing OpenPGP keys within an organization.  Extensive discussions
helped us understand OCCRP's current workflows and the challenges they
face, and by now they have deployed OpenPGP CA on their test
infrastructure and are providing vital feedback.

Other projects developed in close cooperation with users are
[Hagrid](/projects/#hagrid), [Umschlagend](/projects/#umschlagend),
and [sqv](/projects/#sqv).  We reached out to
[Schleuder](https://schleuder.org/) and are looking forward to working
with them.  Since Schleuder is written in Ruby, we started working on
[Ruby bindings for Sequoia](/projects/#ruby-bindings-for-sequoia).

Having a specific use case and people who actually want to use the end
product is a great way to develop a project.

Looking at Sequoia, we want to make it usable for all existing
projects.  To that end, we worked hard to make it
[easy-to-use](/easy-to-use/) from [different
languages](/projects/#language-bindings) and environments.
[Sequoia-OpenPGP](/projects/#sequoia-openpgp) is a very complete and
faithful implementation of RFC4880 (and some extensions).  It is a
versatile toolkit for building OpenPGP-enabled applications.

Experience has shown that imposing opinions on how downstream projects
ought to use OpenPGP leads to friction and inevitably the users
working around the imposed policy.  Therefore, we designed Sequoia to
be policy free and let downstream decide on the semantics they want.

Finally, OpenPGP is about securing communications, and communication
requires a party to communicate with.  Said party could use a
different implementation of OpenPGP.  Therefore, it is in our best
interest to also improve other implementations.  To this end we
created the [OpenPGP interoperability test
suite](/projects/#openpgp-interoperability-test-suite).  This test
suite has been very successful in identifying problems in many OpenPGP
implementations. If you want to see your implementation included in
these results, please get in touch.

---

Read more about how we make Sequoia [secure and
robust](/secure-and-robust/) yet at the same time [easy to
use](/easy-to-use/).
