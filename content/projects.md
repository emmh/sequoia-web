+++
title = "Projects"
keywords = ["code", "git", "related projects", "community"]
+++

There are a number of projects under the Sequoia umbrella, or a
somehow associated with the Sequoia project.

{{< heading 2 "Sequoia-PGP" >}}

{{< project
	name="sequoia-openpgp"
	homepage="https://sequoia-pgp.org"
	repo="https://gitlab.com/sequoia-pgp/sequoia"
	crate="sequoia-openpgp"
	docs="https://docs.sequoia-pgp.org/sequoia_openpgp/"
	bugs="https://gitlab.com/sequoia-pgp/sequoia/issues"
	status="/status/"
	guide="https://docs.sequoia-pgp.org/sequoia_guide"
	contribute="/contribute/"
	donate=""
	summary="OpenPGP data types and associated machinery."
	description="This crate aims to provide a complete implementation of OpenPGP as defined by RFC 4880 as well as some extensions (e.g., RFC 6637, which describes ECC cryptography for OpenPGP.  There are prototypes of crates providing higher-level interfaces.  Take a look at the other crates in the toplevel of the repository."
>}}

{{< project
	name="sqop"
	homepage="https://sequoia-pgp.org"
	repo="https://gitlab.com/sequoia-pgp/sequoia"
	crate=""
	docs="https://docs.sequoia-pgp.org/sqop/"
	bugs="https://gitlab.com/sequoia-pgp/sequoia/issues"
	status=""
	guide=""
	contribute=""
	donate=""
	summary="An implementation of SOP using Sequoia."
	description="<i>sqop</i> implements the <a href='https://datatracker.ietf.org/doc/draft-dkg-openpgp-stateless-cli/'>Stateless OpenPGP Command Line Interface</a> and provides encryption, decryption, signature creation and verification, and basic key and certificate management with a convenient git-style subcommand interface."
>}}

{{< project
	name="sqv"
	homepage="https://sequoia-pgp.org"
	repo="https://gitlab.com/sequoia-pgp/sequoia"
	crate="sequoia-sqv"
	docs="https://docs.sequoia-pgp.org/sqv/"
	bugs="https://gitlab.com/sequoia-pgp/sequoia/issues"
	status=""
	guide=""
	contribute=""
	donate=""
	summary="A command-line OpenPGP signature verification tool."
	description="<i>sqv</i> verifies detached OpenPGP signatures.  It is a replacement for <i>gpgv</i>.  Unlike <i>gpgv</i>, it can take additional constraints on the signature into account.  It is designed for sotware distribution systems.  See <a href='https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=872271'>this bug report</a>."
>}}

{{< project
	name="sq"
	homepage="https://sequoia-pgp.org"
	repo="https://gitlab.com/sequoia-pgp/sequoia"
	crate=""
	docs="https://docs.sequoia-pgp.org/sq/"
	bugs="https://gitlab.com/sequoia-pgp/sequoia/issues"
	status=""
	guide=""
	contribute=""
	donate=""
	summary="A command-line frontend for Sequoia."
	description="<i>sq</i> provides encryption, decryption, signature creation and verification, key and certificate management, and key server and <i>WKD</i> interactions with a convenient git-style subcommand interface."
>}}

{{< heading 2 "Language bindings" >}}

{{< project
	name="C bindings for Sequoia"
	homepage=""
	repo="https://gitlab.com/sequoia-pgp/sequoia"
	crate=""
	docs=""
	bugs="https://gitlab.com/sequoia-pgp/sequoia/issues?label_name[]=ffi"
	status=""
	guide=""
	contribute=""
	donate=""
	summary="Allows the use of Sequoia from the C programming language."
	description=""
>}}

{{< project
	name="Python bindings for Sequoia"
	homepage=""
	repo="https://gitlab.com/sequoia-pgp/sequoia"
	crate=""
	docs=""
	bugs="https://gitlab.com/sequoia-pgp/sequoia/issues?label_name=python"
	status=""
	guide=""
	contribute=""
	donate=""
	summary="Allows the use of Sequoia from the Python programming language."
	description=""
>}}

{{< project
	name="Ruby bindings for Sequoia"
	homepage=""
	repo="https://gitlab.com/dorle/sequoia-ruby-ffi"
	crate=""
	docs=""
	bugs="https://gitlab.com/dorle/sequoia-ruby-ffi/issues"
	status=""
	guide=""
	contribute=""
	donate=""
	summary="Allows the use of Sequoia from the Ruby programming language."
	description=""
>}}

{{< heading 2 "Projects under Sequoia's Umbrella" >}}

{{< project
	name="Hagrid"
	homepage="https://keys.openpgp.org"
	repo="https://gitlab.com/hagrid-keyserver/hagrid"
	crate=""
	docs="https://keys.openpgp.org/about/usage"
	bugs="https://gitlab.com/hagrid-keyserver/hagrid/issues"
	status=""
	guide=""
	contribute=""
	donate=""
	summary="Hagrid is a verifying OpenPGP key server."
	description="Hagrid, notably running on <a href='https://keys.openpgp.org'>keys.openpgp.org</a>, is a public service for the distribution and discovery of OpenPGP-compatible keys, commonly referred to as a <i>keyserver</i>.  In contrast to conventional keysevers, Hagrid does not publish identity information without the consent of the user, and allows the removal of identity information."
>}}

{{< project
	name="OpenPGP CA"
	homepage=""
	repo="https://gitlab.com/openpgp-ca/openpgp-ca"
	crate=""
	docs="https://openpgp-ca.gitlab.io/openpgp-ca/"
	bugs="https://gitlab.com/openpgp-ca/openpgp-ca/issues"
	status=""
	guide=""
	contribute=""
	donate=""
	summary="OpenPGP CA is a tool for managing OpenPGP keys within an organization."
	description="OpenPGP CA's primary goal is to make it trivial for end users to authenticate OpenPGP keys for users in their organization or in an adjacent organization.  In other words, OpenPGP CA makes it possible for users in an organization to securely and seamlessly communicate via PGP-encrypted email using existing email clients and encryption plugins without having to manually compare fingerprints and without having to understand OpenPGP keys or signatures."
>}}

{{< project
	name="Koverto"
	homepage=""
	repo="https://gitlab.com/koverto/koverto"
	crate=""
	docs="https://koverto.gitlab.io/koverto/"
	bugs="https://gitlab.com/koverto/koverto/issues"
	status=""
	guide=""
	contribute=""
	donate=""
	summary="Koverto is an OpenPGP encrypting SMTP Proxy."
	description="Integrating OpenPGP encryption in the mail server makes it easy to encrypt messages from any service.  Koverto signs and encrypts notification emails before it sends them out to the recipients."
>}}

{{< project
	name="OpenPGP interoperability test suite"
	homepage="https://tests.sequoia-pgp.org"
	repo="https://gitlab.com/sequoia-pgp/openpgp-interoperability-test-suite"
	crate=""
	docs=""
	bugs="https://gitlab.com/sequoia-pgp/openpgp-interoperability-test-suite/issues"
	status=""
	guide=""
	contribute=""
	donate=""
	summary="A test suite designed to chart and improve interoperability of OpenPGP implementations."
	description="It uses a simple black-box API implemented by several backends, and maps test over all implementations.  Implementations that implement a subset of the <a href='https://tools.ietf.org/html/draft-dkg-openpgp-stateless-cli-01'>Stateless OpenPGP Command Line Interface</a> can be plugged into the test suite."
>}}

{{< project
	name="dump.sequoia-pgp.org"
	homepage="https://dump.sequoia-pgp.org"
	repo="https://gitlab.com/sequoia-pgp/dump.sequoia-pgp.org"
	crate=""
	docs=""
	bugs="https://gitlab.com/sequoia-pgp/dump.sequoia-pgp.org/issues"
	status=""
	guide=""
	contribute=""
	donate=""
	summary="A PGP packet dumper as a service."
	description=""
>}}

{{< project
	name="Rust bindings for nettle"
	homepage=""
	repo="https://gitlab.com/sequoia-pgp/nettle-rs"
	crate="nettle"
	docs="https://sequoia-pgp.gitlab.io/nettle-rs/nettle/"
	bugs="https://gitlab.com/sequoia-pgp/nettle-rs/issues"
	status=""
	guide=""
	contribute=""
	donate=""
	summary="Rust bindings for the Nettle cryptographic library."
	description=""
>}}

{{< project
	name="pgpcat"
	homepage=""
	repo="https://gitlab.com/sequoia-pgp/pgpcat"
	crate=""
	docs=""
	bugs="https://gitlab.com/sequoia-pgp/pgpcat/issues"
	status=""
	guide=""
	contribute=""
	donate=""
	summary="pgpcat is a simple program to extract the data from an OpenPGP message.  This program does not do any decryption nor does it verify signatures.  In fact, it can't even decompress any compressed data."
	description="pgpcat is a first step towards allowing data to be signed inline by default.  Distributing signatures inline makes it much easier to verify signatures, because there is only a single file to download, and there is no need to determine what the signature file is called (foo.sig? foo.sign?  foo.asc?) or whether the signature data is over the compressed or uncompressed data (for instance, the linux kernel distributes linux.tar.gz, but the signature is over linux.tar)." >}}

{{< project
	name="wkd-checker"
	homepage=""
	repo="https://gitlab.com/wiktor-k/wkd-checker"
	crate=""
	docs=""
	bugs="https://gitlab.com/wiktor-k/wkd-checker/issues"
	status=""
	guide=""
	contribute=""
	donate=""
	summary="Web Key Directory checker provides a lint service for WKD deployments." >}}

{{< checker >}}

{{< heading 2 "Downstream users" >}}

{{< project
	name="Pijul"
	homepage="https://pijul.org/"
	repo="https://nest.pijul.com/pijul_org/pijul"
	crate="pijul"
	docs="https://pijul.org/manual/"
	bugs="https://nest.pijul.com/pijul_org/pijul/discussions"
	status=""
	guide=""
	contribute=""
	donate=""
	summary="Pijul is a free and open source distributed version control system."
	description="Its distinctive feature is to be based on a sound theory of patches, which makes it easy to learn and use, and really distributed.  The main difference between Pijul and Git is that Pijul deals with changes (or patches), whereas Git deals only with snapshots (or versions)."
>}}

{{< project
	name="KIPA"
	repo="https://github.com/mishajw/kipa"
	crate="kipa"
	docs="https://docs.rs/crate/kipa/0.2.10"
	bugs="https://github.com/mishajw/kipa/issues"
	status=""
	guide=""
	contribute=""
	donate=""
	summary="A distributed Key to IP Address query network."
	description="KIPA is a look-up service for finding out which IP addresses belong to a public key. Everyone on the KIPA network allows themselves to be looked up by their key, and is helps to look up others in the network.  It is distributed, meaning that there is no single server on which the network relies.  It is zero-trust, meaning it is resilient against bad actors.  It is scalable, performing well with large network sizes and slow network speeds."
>}}

{{< project
	name="Radicle"
	homepage="https://radicle.xyz/"
	repo="https://github.com/radicle-dev"
	crate=""
	docs="https://radicle.xyz/towards-decentralized-code-collaboration.html"
	bugs="https://github.com/radicle-dev/radicle-link/issues"
	status=""
	guide=""
	contribute=""
	donate=""
	summary="Secure peer-to-peer code collaboration without intermediaries."
	description="Radicle is being built to provide a convenient collaboration workflow without intermedaries or central servers. Issues, patches and code review items can be shared seamlessly between peers and interacted with on the user's machine, just like one interacts with a git repository."
>}}

---

If you want to include a project in this list, please [get in
contact](https://gitlab.com/sequoia-pgp/sequoia-web/issues/).
