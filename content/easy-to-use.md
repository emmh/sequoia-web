+++
title = "Easy to Use"
keywords = ["easy", "ease of use", "development", "integration", "user friendly"]
+++

A library is only as good as its integration in downstream projects.
As such, we made ease of use one of our main goals.

Our first decision in this regard is our library-first approach.  In
contrast with other implementations, we decided to provide a library
that contains all the functionality first, and a command-line frontend
on top of that.  The rationale is that a library offers better
integration into downstream applications, and no user should be
tempted to 'shell out' to our command-line frontend because of a
feature not available in the library.

Now, while Sequoia is implemented in Rust, most software currently out
there is not.  But Rust offers a way to create an API with C calling
conventions, allowing the use from C and other languages.  A Rust
library equipped with such an interface can be used in a large variety
of environments, from kernel space or embedded devices to garbage
collected environments due to the fact that Rust requires little to no
runtime support.

While the C API is necessarily more verbose than the Rust one,
experience with our library has shown that C code using Sequoia is
more compact than code using GPGME, even though Sequoia currently
offers only a [low-level interface](/projects/#sequoia-openpgp),
whereas GPGME offers a [high-level
one](https://gnupg.org/software/gpgme/index.html).

Having a C API allows us to create
[bindings](/projects/#language-bindings) for other programming
languages.  Preliminary bindings for
[Python](/projects/#python-bindings-for-sequoia) and
[Ruby](/projects/#ruby-bindings-for-sequoia) exist, bringing Sequoia's
features and robustness to even more projects.

Often, all you want is to verify an OpenPGP signature.  For example,
OpenPGP signatures secure the software distribution mechanisms of all
major Linux distributions.  We provide [sqv](/projects/#sqv), an
specialized tool to verify detached signatures that is robust and easy
to use.  It allows you to specify signature thresholds (e.g. at least
two valid signatures required), and to restrict the time interval in
which signatures should be considered valid (e.g. newer than X).
Simply looking at the tool's exit status tells you the result of the
verification.

One of the main design aspects of Sequoia's core [OpenPGP
crate](/projects/#sequoia-openpgp) is that we tried hard to avoid
dictating how OpenPGP should be used. This doesn't mean that we don't
have opinions about how OpenPGP should be used in a number of common
scenarios (for instance, message validation). But, in this crate, we
refrain from expressing those opinions.  By keeping the core
policy-free, you don't have to work around an implementations policy
or design decisions.

Of course, there are policy decisions that have to be expressed, for
example if an algorithm is considered insecure, or simply not in
compliance with local regulation.  For this, we abstracted over
concrete policies using our [policy
framework](https://docs.sequoia-pgp.org/sequoia_openpgp/policy/index.html).
A [standard
policy](https://docs.sequoia-pgp.org/sequoia_openpgp/policy/struct.StandardPolicy.html)
is provided that can be further tweaked to fit your needs.

Finally, we want to provide easy-to-use applications for developers
and power-users.  Our command-line frontend [sq](/projects/#sq)
provides encryption, decryption, signature creation and verification,
key and certificate management, and powerful data inspection
capabilities with an easy-to-use git-style interface.

To make it even easier to try Sequoia, we host an [OpenPGP packet
dumper](/projects/#dump-sequoia-pgp-org) on
[https://dump.sequoia-pgp.org](https://dump.sequoia-pgp.org).

---

Read more about how we make Sequoia [secure and
robust](/secure-and-robust/) and what our [holistic
approach](/holistic-approach/) to OpenPGP security looks like.
