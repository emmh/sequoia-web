+++
title = "Contribute"
description = "How to contribute"
keywords = ["contribute", "getting involved", "hacking"]
+++

## Get Sequoia

Sequoia is maintained in a Git repository.  To clone it, do:

```
% git clone https://gitlab.com/sequoia-pgp/sequoia.git
```

## Build Sequoia

Please see
[here](https://gitlab.com/sequoia-pgp/sequoia#building-sequoia) for
build instructions.

## Get in touch

If you are interested in using or developing Sequoia, please get in
touch.  We can be reached on our mailing list, and using IRC.

### Reporting bugs

Our bugtracker can be found
[here](https://gitlab.com/sequoia-pgp/sequoia/issues).

Please report security vulnerabilities to <a
href="mailto:security@sequoia-pgp.org">security@sequoia-pgp.org</a>,
preferably encrypted using this certificate:

    -----BEGIN PGP PUBLIC KEY BLOCK-----
    Comment: 6EFC 2689 8828 74C3 1E4F  C4EC 4D66 CB0F EBA5 DAF1
    Comment: security@sequoia-pgp.org

    xjMEX9DpCRYJKwYBBAHaRw8BAQdAWG3gIChzlo79zulsVYQFU5wySD+PZVRbMuHl
    IGX2FTzCwBEEHxYKAIMFgl/Q6QkFiQlmAYADCwkHCRBNZssP66Xa8UcUAAAAAAAe
    ACBzYWx0QG5vdGF0aW9ucy5zZXF1b2lhLXBncC5vcmdzWyhXcksc6XRJFnUylyG6
    au1sGEr5Sy67X1pc4bLsYQMVCggCmwECHgEWIQRu/CaJiCh0wx5PxOxNZssP66Xa
    8QAAQnMBAP1fvOnMGs8OmuhDgcfuaEGJOdkDiX3clct/5Dyibym9AQCUgWxZoXV2
    pvDmzY/FwqJblZ++DcxC3Crub3+UBiIAC80Yc2VjdXJpdHlAc2VxdW9pYS1wZ3Au
    b3JnwsAUBBMWCgCGBYJf0OkJBYkJZgGAAwsJBwkQTWbLD+ul2vFHFAAAAAAAHgAg
    c2FsdEBub3RhdGlvbnMuc2VxdW9pYS1wZ3Aub3JnY2DIeOiQxRpmC2+d87v2loX4
    QvJy2gVHTM65xU+WA6sDFQoIApkBApsBAh4BFiEEbvwmiYgodMMeT8TsTWbLD+ul
    2vEAADgxAP9maacT175K14ZMgEiZ15dhm4+n5KoN5e7F5sWBvcjb/QD/WfwAx/Va
    DUU7omyRC2w/u2swLE8XN2+g5JofrNRrZwjOOARf0OkJEgorBgEEAZdVAQUBAQdA
    ES2OKI0Y7zefTNUUgp8TpDEt9HyqG8K1jBeRRZRqE1YDAQgJwsAJBBgWCgB7BYJf
    0OkJBYkJZgGACRBNZssP66Xa8UcUAAAAAAAeACBzYWx0QG5vdGF0aW9ucy5zZXF1
    b2lhLXBncC5vcmeo6p8JahC+tYoa/QMgNeSRsFJfsI6aZNQpKLQ8w6KSHwKbhAIe
    ARYhBG78JomIKHTDHk/E7E1myw/rpdrxAAAYmAD/QP/xsyOqU7UKGczfKYQzaCj9
    EGpGL+fWV3w9dSUMSGIA+QGpTnAAJRkuGXbcOZ230oi3YiyjE4UpAiOPpqAGqcUJ
    =e55j
    -----END PGP PUBLIC KEY BLOCK-----


### Mailing list

Join our mailing list by sending a mail to <a
href="mailto:devel-subscribe@lists.sequoia-pgp.org">devel-subscribe@lists.sequoia-pgp.org</a>
or go to [lists.sequoia-pgp.org](https://lists.sequoia-pgp.org).

### IRC

Find us on freenode in <a
href="irc://irc.freenode.net/sequoia">#sequoia</a>.

## License & CLA

Sequoia PGP is owned by the [p≡p foundation] and licensed under the
terms of the GPLv3+.

  [p≡p foundation]: https://pep.foundation/

To finance its mission, privacy by default, the [p≡p foundation]
allows third parties (currently only [p≡p security]) to relicense its
software.  Consistent with the rules of a foundation, the money
collected by the foundation in this manner is fully reinvested in the
foundation's mission, which includes further development of Sequoia
PGP.

  [p≡p security]: https://www.pep.security/

To do this, the [p≡p foundation] needs permission from all
contributors to relicense their changes.  In return, the
[p≡p foundation] guarantees that *all* releases of Sequoia PGP (and
any other software it owns) will also be released under a GNU-approved
license.  That is, even if Foo Corp is granted a license to use
Sequoia PGP in a proprietary product, the exact code that Foo Corp
uses will also be licensed under a GNU-approved license.

If you want to contribute to Sequoia PGP, and you agree to the above,
please sign the [p≡p foundation]'s [CLA].  This is an electronic
assignment; no paper work is required.  You'll need to provide a valid
email address.  After clicking on a link to verify your email address,
you'll receive a second email, which contains the contract between you
and the [p≡p foundation].  Be sure to keep it for future reference.
The maintainers of Sequoia PGP will also receive a notification.  At
that point, we can merge patches from you into Sequoia PGP.

  [CLA]: https://contribution.pep.foundation/contribute/

Please direct questions regarding the CLA to [contribution@pep.foundation].

  [contribution@pep.foundation]: mailto:contribution@pep.foundation
