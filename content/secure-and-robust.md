+++
title = "Secure and Robust"
keywords = ["secure", "robust", "development"]
+++

Sequoia focuses on security and robustness in our choice of tools, our
development methodology, and feature set.

At the core of our efforts lies the choice of programming language,
[Rust](https://www.rust-lang.org/).  Rust's type system provides
strong guarantees that are checked at compile time.  It guarantees
spatial and temporal memory safety, preventing common classes of bugs
like memory leaks, use of stale pointers, out-of-bounds accesses, and
race conditions.  Rust also makes sure that all possible values are
exhaustively handled, notably this ensures a robust error propagation
and handling.  A side effect of choosing Rust is that we started from
a clean plate, carrying no implementational or structural baggage.

Of course, using Rust alone is no panacea.  Our development relies
heavily on tests, and the tests are routinely executed in our
continuous integration setup.  The tests fall in roughly four
categories:

 - Unit tests check basic functionality of small units of code.
 - Roundtrip tests check that we can consume artifacts we produce, and
   that we get the original data back.  The roundtrip tests use
   [randomized data](https://docs.rs/quickcheck), a process similar to
   fuzzing.
 - Integration tests check higher-level functionality.
 - [Interoperability tests](https://tests.sequoia-pgp.org/) check that
   Sequoia can consume artifacts produced by other OpenPGP
   implementations, and that the artifacts we produce can be consumed
   by others.

Matching these ideas of ensuring robustness by construction is our use
of formal grammars to verify the structure of OpenPGP messages,
certificates, and protocol messages exchanged with legacy
implementations.  The formal grammar is turned into code using a
parser generator, resulting in safe and robust handing of potentially
maliciously corrupted data.

In recent times, we have seen the emergence of cross-protection-domain
attacks, i.e. attacks that steal information across
process-boundaries, or even across virtualization domains.  In a world
of cloud-computing, where customers share physical computers, this is
a serious concern.  Some of these attacks, like Spectre or Meltdown,
exploit microarchitectural flaws, while some exploit physical layout
like Rowbleed.  A classical form, sometimes requiring physical access
to the computer, are coldboot attacks, where an adversary reboots the
machine to circumvent the operating systems protection, or even
outright pulls the RAM from the running machine and inserts it into
her own.

Sequoia protects sensitive data, such as secret keys and passwords, by
encrypting them in memory while they are unused, and decrypting them
on demand.  This is implemented in a platform-agnostic way, requiring
no specialized hardware whatsoever.  Read more about the
implementation
[here](https://docs.sequoia-pgp.org/sequoia_openpgp/crypto/mem/struct.Encrypted.html).

Another area of concern are metadata leaks: who communicates with
whom, when, how often, and how much?  These kinds of information tell
an adversary a lot, even if the message itself is encrypted.  Metadata
are notoriously difficult to protect, but that doesn't stop us from
devising mechanisms to protect our users.

As an example for how powerful metadata-based attacks are, the
confidentiality of the Wikimedia Foundation board election [has been
compromised](https://mailarchive.ietf.org/arch/msg/openpgp/rG-X9rp2jlbyACoosnbxRXjCeys/)
by the size of encrypted ballots once.  To prevent this kind of
attack, we implemented a [padding
scheme](https://docs.sequoia-pgp.org/sequoia_openpgp/serialize/padding/index.html)
to obscure the message size in a way that is compatible with other
implementations.

---

Read more about how we make Sequoia [easy to use](/easy-to-use/) and
what our [holistic approach](/holistic-approach/) to OpenPGP security
looks like.
