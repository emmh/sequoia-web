+++
title = "The Sequoia Community"
keywords = ["about us", "who are we", "community"]
+++

A number of people working in the OpenPGP space are involved in the
Sequoia project.  Some work directly on Sequoia, some work on projects
using Sequoia, some are otherwise working on improving the state of
the OpenPGP ecosystem.

{{< portrait src="neal.jpeg" name="Neal" right="right" pep="true"
	description="Neal began working on GnuPG towards the end of his PhD. After two and a half years at g10code, he joined p≡p in 2017 and co-founded the Sequoia project with Justus and Kai. Outside of working to improve the OpenPGP ecosystem and advocating privacy, Neal likes cooking with friends, going to the sauna, and playing with his kids." >}}

{{< portrait src="justus.jpeg" name="Justus" pep="true"
    description="Justus started working on GnuPG after (finally!!) finishing his Diplom.  After hacking on GnuPG, its test suite, and Python bindings for a bit over two years, he joined forces with Kai and Neal to start a new OpenPGP implementation.  In his free time, he likes to tinker, puzzle, hack, and has an ongoing twist with gravity." >}}

{{< portrait src="azul.jpeg" name="Azul" right="right" pep="true"
    description="Azul has been working to make the world a better place. In the last years he has joined several efforts to encrypt more emails. With <a href='https://leap.se'>leap.se</a> he improved tooling for providers to support their users use of OpenPGP. In the context of the <a href='https://nextleap.eu'>nextleap.eu</a> research project, he discussed and implemented mechanisms for key distribution and validation. He's also involved with <a href='https://autocrypt.org'>Autocrypt</a>" >}}

{{< portrait src="igor.jpeg" name="Igor" pep="true"
	description="Igor is a part of the <a href='https://www.rust-lang.org/governance/teams/dev-tools'>Rust dev-tools team</a>. He doesn't stop until everything is successfully (re)written in pure Rust - including writing Windows CNG support for Sequoia." >}}

{{< portrait src="dkg.jpeg" name="dkg"
    description="After years of working with the universal free software operating system, dkg became a <a href='https://www.debian.org'>Debian</a> Developer in 2009.  He is currently employed by the <a href='https://www.aclu.org'>ACLU</a> as a Senior Staff Technologist in the ACLU Speech, Privacy, and Technology Project. One of his major focuses is improving the OpenPGP ecosystem. In particular, he has spent much time over the past decade working on email encryption, securing the distribution of software, improving tooling, and helping standardization efforts in formal organizations like the <a href='https://www.ietf.org'>IETF</a>, as well as informal collaborations like <a href='https://autocrypt.org'>Autocrypt</a>.  dkg has a fur hat. The fur was given willingly by a family friend's <a href='https://www.greatpyratlanta.com'>rescue dog</a>, who liked to be brushed." right="right" >}}

{{< portrait src="vincent.jpeg" name="Vincent"
    description="Vincent has been active in the OpenPGP community since 2015.  He started by developing <a href='https://www.openkeychain.org'>OpenKeychain</a> for Android, and integrating it into K-9 Mail. More recently, he co-founded the <a href='https://autocrypt.org'>Autocrypt</a> project, which automates public key management between email clients.  In 2019, he worked together with the Sequoia team to launch <a href='https://keys.openpgp.org'>keys.openpgp.org</a> as an alternative keyserver. You should look at his amazin' horse." >}}

{{< portrait src="heiko.jpeg" name="Heiko" right="right"
    description="Heiko cares deeply about free software and privacy. In 2018, he decided to take a more active role in realizing these ideas and began work on <a href='https://gitlab.com/hkos/openpgp-ca'>OpenPGP CA</a>, a tool to help organizations use OpenPGP in a more secure manner with much less effort. Heiko lives on planet Earth.</p><p>His work is funded by the <a href='https://nlnet.nl/'>NLnet Foundation</a>." >}}

{{< portrait src="daniel.jpeg" name="Daniel"
    description="Daniel is a long-time free software developer. He doesn't only start free software projects, but he has a history of contributing to existing efforts. Currently, his main focus is on <a href='https://rustup.rs'>rustup</a>, Rust's installer. He's working to secure Rust's software distribution by improving the project's use of digital signatures. His first DebConf was DebConf 3 in Oslo, Norway. He runs, for his sins." >}}

# Sequoia's founding team

In 2017, three former GnuPG developers ---Neal, Kai, and Justus--- set
out to create a new opinionated OpenPGP implementation.

<div style="text-align: center; margin: 3em 0">
	<img src="/img/team.jpeg" style="max-width: 100%"
	     alt="Neal, Kai, and Justus from the Sequoia team" />
</div>
