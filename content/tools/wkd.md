+++
title = "WKD Deployment Linter"
keywords = ["wkd", "checke", "linter", "deployment"]
+++

{{< project
	name="wkd-checker"
	homepage=""
	repo="https://gitlab.com/wiktor-k/wkd-checker"
	crate=""
	docs=""
	bugs="https://gitlab.com/wiktor-k/wkd-checker/issues"
	status=""
	guide=""
	contribute=""
	donate=""
	summary="Web Key Directory checker provides a lint service for WKD deployments." >}}

{{< checker >}}
