---
title: "State of the Seedling"
author: Justus
date: 2018-09-07T15:11:49+02:00
banner: img/sequoia-banner.jpeg
---

Hey folks :)

I'd like to inform you about the recent progress on your favorite
OpenPGP implementation.  It has been five weeks since my last report,
and there are exciting developments to report.

<!--more-->

Our parser is now more robust.  If a packet stream is damaged, it
tries to find the next OpenPGP frame, and resumes parsing.  This helps
to deal with corrupted data returned from key servers.

Sequoia now features an API to generate keys and construct a TPK
structure.  This is a medium-level API that takes care of creating the
binding signatures.

ASCII Armor, designed to protect OpenPGP data in transit, has been a
source of problems if the armor structure is damaged.  For example,
copying data manually from one program to another might introduce or
drop newlines.  Our armor parser now simply tries to detect
base64-encoded OpenPGP data.  This way, if the framing is in any way
damaged, or even stripped, we can still recover the message.

Our detached signature verification tool, sqv, now supports
--not-before and --not-after options that constrain the signatures
that are considered valid to the given interval.  Furthermore, it
properly ignores duplicate signatures, and multiple signatures from
the same TPK.

Sequoia can now generate and parse the proposed intended recipient
subpacket.  Support for emitting the subpacket is implemented in the
streaming signer.  Since Sequoia currently lacks a streaming
decryption and verification interface, the counterpart of checking the
intended recipients is not yet implemented.

We have been working on the usability of our command line frontends.
It is no longer necessary to use -i to specify the input file.  We now
generate shell completions for both sq and sqv for all shells
supported by our command line parser (bash, fish, zsh, PowerShell, and
elvish).  sq gained support for signature generation and verification.
Keys for all cryptographic operations can now be given as files.  sq
dump now pretty-prints the packets, similar to pgpdump.

We now generate a pkgconfig file for the debug build.  Linking against
Sequoia from C during development is now as easy as pointing
PKG_CONFIG_PATH to sequoia/target/debug.

Sequoia is now more efficient in terms of stack use when parsing
multiple signatures, and nested signatures (i.e. notarizations) can
now be checked.

Sequoia gained a streaming signature verification filter.  Streaming
verification (and decryption) has been missing until now, because
designing an API that is convenient yet reasonably safe has been
challenging, and the verifying filter is our testbed for the more
complex case of a decrypting and verifying filter.

We cleaned up our interface, and did some refactoring to get rid of
accumulated technical debt.  Notably our packet structs are now opaque
and have appropriate accessors, and the structure of the openpgp crate
is cleaner and hopefully a bit more accessible.

Our TPK module can now merge incomplete TPKs.  Such TPKs may be handed
out by a privacy-preserving keyserver that omits UserIDs when clients
poll for key updates.

Kai published his ongoing work on our keyserver.  The keyserver aims
at being more performant, and more privacy preserving than the SKS
keyserver network.  It can be found here:
https://gitlab.com/sequoia-pgp/garbage-pile

Neal cut the recordings of our talks at the Delta X gathering, and
they can be found on our web site.

Finally, this is the first of these reports that is also published in
our blog in the hope that this is more accessible for the casual
reader.

That's it for now, thanks for flying with Sequoia :)
