---
title: "State of the Seedling"
author: Justus
date: 2018-12-10T13:50:35+01:00
banner: img/sequoia-banner.jpeg
---
I'd like to inform you about the recent progress on your favorite
OpenPGP implementation.  It has been eight weeks since I last wrote an
update, and there are exciting developments to report.

<!--more-->

First of all, the Sequoia project made their [first release] at
RustConf Rome during Neal's talk ([video], [slides]) about our
experiences with Rust.  The release includes only the low-level
`openpgp` crate, which we renamed to [`sequoia-openpgp`] to avoid a
namespace collision.

[first release]: https://sequoia-pgp.org/blog/2018/11/26/initial-release/
[video]: https://media.ccc.de/v/rustfest-rome-6-sequoia
[slides]: https://sequoia-pgp.org/talks/2018-11-rustfest-rome/sequoia-challenges-rustfest.pdf
[`sequoia-openpgp`]: https://crates.io/crates/sequoia-openpgp

In the weeks prior to the release, we moved a lot of code around, and
refined our API.  For example, we introduced a [`crypto`] module and
moved all low-level crypto primitives there.

[`crypto`]: https://docs.sequoia-pgp.org/sequoia_openpgp/crypto/index.html

When generating keys, we now always generate a revocation certificate
in the hope that it will encourage the callee to store it for later
use.  Furthermore, our handling of revocations has been improved.

We released the [bindings] for our cryptographic library, Nettle, and
made Sequoia use the released version.  We fixed a corner-case with
Cv25519 keys encoded using OpenPGP.

[bindings]: https://crates.io/crates/nettle

Our command-line frontend `sq` gained a key generation subcommand, and
is now able to verify detached signatures.

Our C API got a significant number of new functions.

Finally, our detached signature verifiaction tool [sqv] was moved to
its own crate.  As it only uses the low-level [`sequoia-openpgp`]
crate, we were able to release it on crates.io.  Furthermore, since it
doesn't operate on compressed data, we are able to disable
decompression support in the [`sequoia-openpgp`] crate, reducing the
trusted computing base and simplifying packaging of the dependencies.
Work has begun to [package] it for Debian.

[sqv]: https://crates.io/crates/sequoia-sqv
[package]: https://gitlab.com/sequoia-pgp/sequoia/issues/81

That's all for today, and thanks for flying Sequoia!
