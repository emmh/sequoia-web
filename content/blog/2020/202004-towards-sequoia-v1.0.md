---
title: "Towards Sequoia v1.0"
author: Neal
date: 2020-04-26T14:01:39+00:00
banner: img/sequoia-banner.jpeg
---

For the past few weeks, Sequoia has been in a feature freeze as we
[make final preparations] for the 1.0 release.  This is an exciting
time for us.  Although there are [already][already] [several][several] [users][users] [of][of]
[our][our] [software][software], we look forward to offering them [a stable API], and
the promise of security updates.  And, we hope that a 1.0 release will
generate more interest in Sequoia and, consequently, more
opportunities to collaborate with other projects.

  [make final preparations]: https://gitlab.com/sequoia-pgp/sequoia/-/issues?milestone_title=v1.0
  [already]: https://pep.foundation/dev/repos/pEpEngine
  [several]: https://keys.openpgp.org
  [users]: https://gitlab.com/openpgp-ca/openpgp-ca
  [of]: https://gitlab.com/koverto/koverto
  [our]: https://pijul.org/
  [software]: https://crates.io/crates/kipa
  [a stable API]: https://github.com/radicle-dev/radicle-link/issues/20

Currently, we are carefully going through Sequoia's public API and
making sure the [documentation] reflects what Sequoia actually does
(and, conversely, that Sequoia does what it is supposed to do!), and
adding examples.  This work has not only tremendously improved the
documentation, but it has helped to [polish the API], and [has
uncovered several bugs].

  [documentation]: https://docs.sequoia-pgp.org/sequoia_openpgp/
  [polish the API]: https://gitlab.com/sequoia-pgp/sequoia/-/issues/491
  [has uncovered several bugs]: https://gitlab.com/sequoia-pgp/sequoia/-/issues/488
