---
title: "The Seedling Sees the Light of Day"
date: 2018-11-26T10:08:17+01:00
banner: img/sequoia-banner.jpeg
---

The Sequoia team proudly presents the first release of a new, cool
OpenPGP implementation.

On October 16, 2017, we made the first commit to the Sequoia
repository.  Just over a year and a thousand commits later, Sequoia's
low-level API is nearly feature complete, and is already usable.  For
instance, a port of the p≡p engine to Sequoia is almost finished, and
the code is significantly simpler than the version using the current
OpenPGP library.  We've also made experimental ports of other software
that use OpenPGP, and written some new software to further validate
the completeness and ergonomics of the API.

During a successful talk at RustFest Rome presenting Sequoia, and the
challenges of implementing Sequoia in Rust, we officially released
version 0.1 of Sequoia.

 - https://rome.rustfest.eu/sessions/sequoia
 - https://media.ccc.de/v/rustfest-rome-6-sequoia

About Sequoia: Sequoia is developed by three former GnuPG developers,
Neal H. Walfield, Justus Winter, and Kai Michaelis.  To mitigate many
common security problems, Sequoia is written in Rust, a strongly typed
language, which provides temporal and spatial memory safety.  Rust
also provides excellent support for embedding libraries in other
languages.  Sequoia already provides C bindings, and Python bindings
are being actively developed.

Sequoia development is primarily funded by the p≡p Foundation with
additional support from the Wau Holland Foundation.  Contributions are
welcome.  Financial contributions can be made via the p≡p Foundation.
People interested in making technical contributions are welcome to
reach out on IRC, or the mailing list.

 - https://pep.foundation/
 - https://www.wauland.de/
 - https://pep.foundation/support-pep/index.html
 - https://sequoia-pgp.org/contribute/

Additional information about Sequoia can be found on our website, and
in a recent presentation introducing Sequoia:

 - https://sequoia-pgp.org/
 - https://www.youtube.com/watch?v=NBbtIZipeNI


