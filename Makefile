# Makefile for Sequoia-www.

# Tools.
HUGO		?= hugo
HUGO_FLAGS	?=
RSYNC		?= rsync
RSYNC_FLAGS	?=

# Configuration.
TARGET		?= sequoia-pgp.org:sequoia-pgp.org
pygmentsStyle	?= $(shell grep '^pygmentsStyle' config.toml \
                     | cut -d'"' -f2)

.PHONY: all
all: build

static/%: static.in/%
	echo "/* This combines $</*. */" >$@
	echo "/* Please see here for the originals: https://gitlab.com/sequoia-pgp/sequoia-web/tree/master/static.in/ */" >>$@
	for F in $(shell ls -1 $</* | sort); do (printf "\n\n/* $$F: */\n"; cat $$F | sed -e 's/  */ /g' -e 's/$$//' ) >>$@; done

static/style.css: static.in/style.css/syntax.css
static.in/style.css/syntax.css:
	# We cut out the background b/c it doesn't quite match our
	# background color.
	hugo gen chromastyles --style=$(pygmentsStyle) \
		| grep -v Background >$@

.PHONY: build
build: static/style.css static/script.js
	$(HUGO) $(HUGO_FLAGS)
	rm -rf public/css public/js
	find public/img -type f | while read F ; do \
	    if [ -z "$$(find static/img -name $$(basename $$F))" ]; then \
	        rm $$F ; \
	    fi ; \
	done

.PHONY: server
server:
	$(HUGO) server

.PHONY: deploy
deploy: build
	$(RSYNC) $(RSYNC_FLAGS) -r public/* $(TARGET)
